package main

import (
	"gitlab.com/kylesferrazza/goboggle"

	"flag"
	"log"
	"math/rand"
	"os"
	"time"
)

var (
	doPrint      bool
	imagePath    string
	fontFilename string
	fontSize     float64
)

func init() {
	flag.BoolVar(&doPrint, "print-board", false, "print a board")
	flag.StringVar(&imagePath, "filename", "", "output filename for PNG")
	flag.StringVar(&fontFilename, "font-filename", "", "filename of font to use for board")
	flag.Float64Var(&fontSize, "font-size", 90, "size of font to use for board")
}

func main() {
	flag.Parse()
	rand.Seed(time.Now().UnixNano())
	board := goboggle.GenBoard(goboggle.Normal)
	if doPrint {
		goboggle.PrintBoard(board)
	}
	if imagePath != "" {
		if fontFilename == "" {
			log.Fatal("no font filename given!")
		}
		f, err := os.Create(imagePath)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		err = goboggle.GenImage(board, fontFilename, fontSize, f)
		if err != nil {
			panic(err)
		}
	}
}
