package goboggle

var normalCubes = [...][6]string{
	{"A", "A", "E", "E", "G", "N"},
	{"E", "L", "R", "T", "T", "Y"},
	{"A", "O", "O", "T", "T", "W"},
	{"A", "B", "B", "J", "O", "O"},
	{"E", "H", "R", "T", "V", "W"},
	{"C", "I", "M", "O", "T", "U"},
	{"D", "I", "S", "T", "T", "Y"},
	{"E", "I", "O", "S", "S", "T"},
	{"D", "E", "L", "R", "V", "Y"},
	{"A", "C", "H", "O", "P", "S"},
	{"H", "I", "M", "N", "QU", "U"},
	{"E", "E", "I", "N", "S", "U"},
	{"E", "E", "G", "H", "N", "W"},
	{"A", "F", "F", "K", "P", "S"},
	{"H", "L", "N", "N", "R", "Z"},
	{"D", "E", "I", "L", "R", "X"},
}

var oldNormalCubes = [...][6]string{
	{"A", "A", "C", "I", "O", "T"},
	{"A", "H", "M", "O", "R", "S"},
	{"E", "G", "K", "L", "U", "Y"},
	{"A", "B", "I", "L", "T", "Y"},
	{"A", "C", "D", "E", "M", "P"},
	{"E", "G", "I", "N", "T", "V"},
	{"G", "I", "L", "R", "U", "W"},
	{"E", "L", "P", "S", "T", "U"},
	{"D", "E", "N", "O", "S", "W"},
	{"A", "C", "E", "L", "R", "S"},
	{"A", "B", "J", "M", "O", "QU"},
	{"E", "E", "F", "H", "I", "Y"},
	{"E", "H", "I", "N", "P", "S"},
	{"D", "K", "N", "O", "T", "U"},
	{"A", "D", "E", "N", "V", "Z"},
	{"B", "I", "F", "O", "R", "X"},
}

var bigCubes = [...][6]string{
	{"A", "A", "A", "F", "R", "S"},
	{"A", "A", "E", "E", "E", "E"},
	{"A", "A", "F", "I", "R", "S"},
	{"A", "D", "E", "N", "N", "N"},
	{"A", "E", "E", "E", "E", "M"},
	{"A", "E", "E", "G", "M", "U"},
	{"A", "E", "G", "M", "N", "N"},
	{"A", "F", "I", "R", "S", "Y"},
	{"B", "J", "K", "QU", "X", "Z"},
	{"C", "C", "E", "N", "S", "T"},
	{"C", "E", "I", "I", "L", "T"},
	{"C", "E", "I", "L", "P", "T"},
	{"C", "E", "I", "P", "S", "T"},
	{"D", "D", "H", "N", "O", "T"},
	{"D", "H", "H", "L", "O", "R"},
	{"D", "H", "L", "N", "O", "R"},
	{"D", "H", "L", "N", "O", "R"}, // (DUPLICATE)
	{"E", "I", "I", "I", "T", "T"},
	{"E", "M", "O", "T", "T", "T"},
	{"E", "N", "S", "S", "S", "U"},
	{"F", "I", "P", "R", "S", "Y"},
	{"G", "O", "R", "R", "V", "W"},
	{"I", "P", "R", "R", "R", "Y"},
	{"N", "O", "O", "T", "U", "W"},
	{"O", "O", "O", "T", "T", "U"},
}

var superCubes = [...][6]string{
	{"A", "A", "A", "F", "R", "S"},
	{"A", "A", "E", "E", "E", "E"},
	{"A", "A", "E", "E", "O", "O"},
	{"A", "A", "F", "I", "R", "S"},
	{"A", "B", "D", "E", "I", "O"},
	{"A", "D", "E", "N", "N", "N"},
	{"A", "E", "E", "E", "E", "M"},
	{"A", "E", "E", "G", "M", "U"},
	{"A", "E", "G", "M", "N", "N"},
	{"A", "E", "I", "L", "M", "N"},
	{"A", "E", "I", "N", "O", "U"},
	{"A", "F", "I", "R", "S", "Y"},
	{"AN", "ER", "HE", "IN", "QU", "TH"},
	{"B", "B", "J", "K", "X", "Z"},
	{"C", "C", "E", "N", "S", "T"},
	{"C", "D", "D", "L", "N", "N"},
	{"C", "E", "I", "I", "T", "T"},
	{"C", "E", "I", "P", "S", "T"},
	{"C", "F", "G", "N", "U", "Y"},
	{"D", "D", "H", "N", "O", "T"},
	{"D", "H", "H", "L", "O", "R"},
	{"D", "H", "H", "N", "O", "W"},
	{"D", "H", "L", "N", "O", "R"},
	{"E", "H", "I", "L", "R", "S"},
	{"E", "I", "I", "L", "S", "T"},
	{"E", "I", "L", "P", "S", "T"},
	{"E", "I", "O", " ", " ", " "},
	{"E", "M", "T", "T", "T", "O"},
	{"E", "N", "S", "S", "S", "U"},
	{"G", "O", "R", "R", "V", "W"},
	{"H", "I", "R", "S", "T", "V"},
	{"H", "O", "P", "R", "S", "T"},
	{"I", "P", "R", "S", "Y", "Y"},
	{"J", "K", "QU", "W", "X", "Z"},
	{"N", "O", "O", "T", "U", "W"},
	{"O", "O", "O", "T", "T", "U"},
}
