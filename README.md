# goboggle

A Go library for generating [Boggle][boggle] boards as text and images.

[boggle]: https://en.wikipedia.org/wiki/Boggle

## Running example

```
go run ./examples -filename /dev/stdout -font-filename /home/kyle/Downloads/static/PlayfairDisplay-SemiBold.ttf | feh -
```
