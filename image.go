package goboggle

import (
	"errors"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"io"
	"io/ioutil"

	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
)

func doDrawing(fontFilename string, board [][]string, fontSize float64) (error, *image.RGBA) {
	fontBytes, err := ioutil.ReadFile(fontFilename)
	if err != nil {
		return err, nil
	}
	f, err := freetype.ParseFont(fontBytes)
	if err != nil {
		return err, nil
	}

	opts := truetype.Options{
		Size: fontSize,
	}
	face := truetype.NewFace(f, &opts)

	fg, bg := image.Black, image.White
	ruler := color.RGBA{0x00, 0x00, 0x00, 0xff}

	oneCell := 150

	thickness := 5
	var halfThickness int = thickness / 2

	heightCells := len(board)
	widthCells := len(board[0])

	totalHeight := oneCell*heightCells + halfThickness
	totalWidth := oneCell*widthCells + halfThickness

	rgba := image.NewRGBA(image.Rect(0, 0, totalWidth, totalHeight))
	draw.Draw(rgba, rgba.Bounds(), bg, image.ZP, draw.Src)

	var dpi float64 = 72

	c := freetype.NewContext()
	c.SetDPI(dpi)
	c.SetFont(f)
	c.SetFontSize(fontSize)
	c.SetClip(rgba.Bounds())
	c.SetDst(rgba)
	c.SetSrc(fg)
	c.SetHinting(font.HintingNone)

	for xVal := 0; xVal < totalWidth; xVal += oneCell {
		for y := 0; y < totalHeight; y++ {
			for t := 0; t < thickness; t++ {
				rgba.Set(xVal+t-halfThickness, y, ruler)
			}
		}
	}

	for yVal := 0; yVal < totalHeight; yVal += oneCell {
		for x := 0; x < totalWidth; x++ {
			for t := 0; t < thickness; t++ {
				rgba.Set(x, yVal+t-halfThickness, ruler)
			}
		}
	}

	bounds, _, _ := face.GlyphBounds('A')
	ascent := -bounds.Min.Y
	descent := bounds.Max.Y
	body := ascent + descent
	yOffset := body.Round()

	for rowNum, row := range board {
		for colNum, letters := range row {
			xPos := colNum * oneCell
			yPos := rowNum*oneCell + yOffset

			curTotalWidth := 0
			for i, letter := range letters {
				if i > 0 {
					// A guess to the number of pixels between characters..
					curTotalWidth += 10
				}
				bounds, _, ok := face.GlyphBounds(letter)
				if ok != true {
					return errors.New("error centering text"), nil
				}
				curTotalWidth += (bounds.Max.X.Round() - bounds.Min.X.Round())
			}

			xPos += (oneCell / 2) - (curTotalWidth / 2)
			yPos += (oneCell / 2) - (yOffset / 2)

			pt := freetype.Pt(xPos, yPos)
			_, err = c.DrawString(letters, pt)
			if err != nil {
				return err, nil
			}
		}
	}

	return nil, rgba
}

func GenImage(board [][]string, fontFilename string, fontSize float64, writer io.Writer) error {
	err, img := doDrawing(fontFilename, board, fontSize)
	if err != nil {
		return err
	}

	if err := png.Encode(writer, img); err != nil {
		return err
	}
	return nil
}
