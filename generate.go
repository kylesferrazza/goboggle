package goboggle

import (
	"fmt"
	"math/rand"
)

type BoggleType int

const (
	Normal BoggleType = iota
	OldNormal
	Big
	BigChallenge
	Super
)

// Generate a boggle board. Seed the random generator before calling this method.
func GenBoard(boardType BoggleType) [][]string {
	var cubes [][6]string
	switch boardType {
	case Normal:
		// Because go is annoying, these cannot be constants
		cubes = normalCubes[:]
	case OldNormal:
		cubes = oldNormalCubes[:]
	case Big, BigChallenge:
		cubes = bigCubes[:]
	case Super:
		cubes = superCubes[:]
	}

	length := len(cubes)

	var size int
	switch length {
	case 36:
		size = 6
	case 25:
		size = 5
	case 16:
		size = 4
	default:
		panic("Invalid cube count")
	}

	perm := rand.Perm(length)

	board := make([][]string, size)
	for row := 0; row < size; row++ {
		board[row] = make([]string, size)
		for col := 0; col < size; col++ {
			i := row*size + col
			board[row][col] = cubes[perm[i]][rand.Intn(6)]
		}
	}

	if boardType == BigChallenge {
		i := rand.Intn(6)
		bigBoggleChallengeCube := []string{"ER", "TH", "IN", "AN", "HE", "QU"}
		roll := bigBoggleChallengeCube[i]

		row := rand.Intn(size)
		col := rand.Intn(size)
		board[row][col] = roll
	}

	return board
}

func PrintBoard(board [][]string) {
	size := len(board)
	for row := 0; row < size; row++ {
		for col := 0; col < size; col++ {
			fmt.Printf("%-2s ", board[row][col])
		}
		fmt.Printf("\n")
	}
}
