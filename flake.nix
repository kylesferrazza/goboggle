{
  description = "goboggle - a go library for generating boggle boards";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      config.allowUnfree = true;
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "goboggle";
      buildInputs = with pkgs; [
        go
        gopls
      ];
      FONT_FILENAME = "${pkgs.corefonts}/share/fonts/truetype/times.ttf";
    };
  };
}
